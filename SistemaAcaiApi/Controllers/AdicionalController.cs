﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SistemaAcaiBusiness.Interfaces;
using SistemaAcaiDomain.Dto;
using System.Collections.Generic;
using System.Linq;

namespace SistemaAcaiApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdicionalController : ControllerBase
    {
        private readonly IAdicionalBusiness adicionalBusiness;

        public AdicionalController(IAdicionalBusiness adicionalBusiness)
        {
            this.adicionalBusiness = adicionalBusiness;
        }

        /// <summary>
        /// Consulta os registros de adicional
        /// 
        /// > GET: api/Adicional
        /// </summary>
        /// <returns>Lista de registros de adicionais encontrados no banco de dados</returns>
        /// <response code="200">Registros encontrados</response>
        /// <response code="404">Nenhum registro encontrado.</response>
        [HttpGet]
        [ProducesResponseType(typeof(AdicionalDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult<IEnumerable<AdicionalDto>> Get()
        {
            var adicionais = adicionalBusiness.Get();
            if (adicionais == null || !adicionais.Any())
            {
                return NotFound();
            }

            return new ObjectResult(adicionais);
        }

        /// <summary>
        /// Consulta um registro de adicional por id
        /// 
        /// > GET: api/Adicional/5
        /// </summary>
        /// <param name="id">Id do registro a ser consultado</param>
        /// <returns>Registro de adicional do id consultado</returns>
        /// <response code="200">Registro encontrado</response>
        /// <response code="404">Registro não encontrado.</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(AdicionalDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult<AdicionalDto> Get(int id)
        {
            var adicional = adicionalBusiness.Get(id);
            if (adicional == null)
            {
                return NotFound();
            }

            return new ObjectResult(adicional);
        }

        /// <summary>
        /// Insere um registro de adicional
        /// 
        /// > POST: api/Adicional
        /// </summary>
        /// <param name="adicional">Dados do registro a ser inserido</param>
        /// <returns></returns>
        /// <response code="200">Registro inserido</response>
        [HttpPost]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        public ActionResult Post([FromBody] AdicionalDto adicional)
        {
            adicionalBusiness.Insert(adicional);
            return Ok();
        }

        /// <summary>
        /// Edita um registro de adicional
        /// 
        /// > POST: api/Adicional/5
        /// </summary>
        /// <param name="id">Id do registro a ser editado</param>
        /// <param name="adicional">Dados do registro a serem alterados, os dados não alterados também devem ser informados</param>
        /// <returns></returns>
        /// <response code="200">Registro editado</response>
        /// <response code="404">Registro não encontrado.</response>
        [HttpPost("{id}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult Edit(int id, [FromBody] AdicionalDto adicional)
        {
            try
            {
                adicionalBusiness.Edit(id, adicional);
            }
            catch (System.Exception)
            {
                return NotFound();
            }

            return Ok();
        }

        /// <summary>
        /// Exclui um registro de adicional
        /// 
        /// > DELETE: api/Adicional/5
        /// </summary>
        /// <param name="id">Id do registro a ser excluído</param>
        /// <returns></returns>
        /// <response code="200">Registro excluído</response>
        /// <response code="404">Registro não encontrado.</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult Delete(int id)
        {
            try
            {
                adicionalBusiness.Delete(id);
            }
            catch (System.Exception)
            {
                return NotFound();
            }

            return Ok();
        }
    }
}
