﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SistemaAcaiBusiness.Interfaces;
using SistemaAcaiDomain.Dto;
using System.Collections.Generic;
using System.Linq;

namespace SistemaAcaiApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PedidoAdicionalController : ControllerBase
    {
        private readonly IPedidoAdicionalBusiness pedidoAdicionalBusiness;

        public PedidoAdicionalController(IPedidoAdicionalBusiness pedidoAdicionalBusiness)
        {
            this.pedidoAdicionalBusiness = pedidoAdicionalBusiness;
        }

        /// <summary>
        /// Consulta os registros de adicionais de pedido
        /// 
        /// > GET: api/PedidoAdicional
        /// </summary>
        /// <returns>Lista de registros de adicionais de pedido encontrados no banco de dados</returns>
        /// <response code="200">Registros encontrados</response>
        /// <response code="404">Nenhum registro encontrado.</response>
        [HttpGet]
        [ProducesResponseType(typeof(PedidoAdicionalDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult<IEnumerable<PedidoAdicionalDto>> Get()
        {
            var pedidos = pedidoAdicionalBusiness.Get();
            if (pedidos == null || !pedidos.Any())
            {
                return NotFound();
            }

            return new ObjectResult(pedidos);
        }

        /// <summary>
        /// Consulta um registro de adicional de pedido por id
        /// 
        /// > GET: api/PedidoAdicional/5
        /// </summary>
        /// <param name="id">Id do registro a ser consultado</param>
        /// <returns>Registro de adicional de pedido do id consultado</returns>
        /// <response code="200">Registro encontrado</response>
        /// <response code="404">Registro não encontrado.</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(PedidoAdicionalDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult<PedidoAdicionalDto> Get(int id)
        {
            var pedido = pedidoAdicionalBusiness.Get(id);
            if (pedido == null)
            {
                return NotFound();
            }

            return new ObjectResult(pedido);
        }

        /// <summary>
        /// Insere um registro de adicional de pedido
        /// 
        /// > POST: api/PedidoAdicional
        /// </summary>
        /// <param name="pedidoAdicional">Dados do registro a ser inserido</param>
        /// <returns>Id do adicional de pedido inserido</returns>
        /// <response code="200">Registro inserido</response>
        [HttpPost]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        public ActionResult Post([FromBody] PedidoAdicionalDto pedidoAdicional)
        {
            var idPedidoAdicional = pedidoAdicionalBusiness.Insert(pedidoAdicional);
            return Ok(idPedidoAdicional);
        }

        /// <summary>
        /// Edita um registro de adicional de pedido
        /// 
        /// > POST: api/PedidoAdicional/5
        /// </summary>
        /// <param name="id">Id do registro a ser editado</param>
        /// <param name="pedidoAdicional">Dados do registro a serem alterados, os dados não alterados também devem ser informados</param>
        /// <returns></returns>
        /// <response code="200">Registro editado</response>
        /// <response code="404">Registro não encontrado.</response>
        [HttpPost("{id}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult Edit(int id, [FromBody] PedidoAdicionalDto pedidoAdicional)
        {
            try
            {
                pedidoAdicionalBusiness.Edit(id, pedidoAdicional);
            }
            catch (System.Exception)
            {
                return NotFound();
            }

            return Ok();
        }

        /// <summary>
        /// Exclui um registro de adicional de pedido
        /// 
        /// > DELETE: api/PedidoAdicional/5
        /// </summary>
        /// <param name="id">Id do registro a ser excluído</param>
        /// <returns></returns>
        /// <response code="200">Registro excluído</response>
        /// <response code="404">Registro não encontrado.</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult Delete(int id)
        {
            try
            {
                pedidoAdicionalBusiness.Delete(id);
            }
            catch (System.Exception)
            {
                return NotFound();
            }

            return Ok();
        }
    }
}
