﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SistemaAcaiBusiness.Interfaces;
using SistemaAcaiDomain.Dto;
using SistemaAcaiDomain.Entities.Projections;
using System.Collections.Generic;
using System.Linq;

namespace SistemaAcaiApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PedidoController : ControllerBase
    {
        private readonly IPedidoBusiness pedidoBusiness;

        public PedidoController(IPedidoBusiness pedidoBusiness)
        {
            this.pedidoBusiness = pedidoBusiness;
        }

        /// <summary>
        /// Consulta os registros de pedido
        /// 
        /// > GET: api/Pedido
        /// </summary>
        /// <returns>Lista de registros de pedidos encontrados no banco de dados</returns>
        /// <response code="200">Registros encontrados</response>
        /// <response code="404">Nenhum registro encontrado.</response>
        [HttpGet]
        [ProducesResponseType(typeof(PedidoInfo), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult<IEnumerable<PedidoInfo>> Get()
        {
            var pedidos = pedidoBusiness.Get();
            if (pedidos == null || !pedidos.Any())
            {
                return NotFound();
            }

            return new ObjectResult(pedidos);
        }

        /// <summary>
        /// Consulta um registro de pedido por id
        /// 
        /// > GET: api/Pedido/5
        /// </summary>
        /// <param name="id">Id do registro a ser consultado</param>
        /// <returns>Registro de pedido do id consultado</returns>
        /// <response code="200">Registro encontrado</response>
        /// <response code="404">Registro não encontrado.</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(PedidoInfo), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult<PedidoInfo> Get(int id)
        {
            var pedido = pedidoBusiness.Get(id);
            if (pedido == null)
            {
                return NotFound();
            }

            return new ObjectResult(pedido);
        }

        /// <summary>
        /// Insere um registro de pedido
        /// 
        /// > POST: api/Pedido
        /// </summary>
        /// <param name="pedido">Dados do registro a ser inserido</param>
        /// <returns>Id do pedido inserido</returns>
        /// <response code="200">Registro inserido</response>
        [HttpPost]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        public ActionResult Post([FromBody] PedidoDto pedido)
        {
            var idPedido = pedidoBusiness.Insert(pedido);
            return Ok(idPedido);
        }

        /// <summary>
        /// Edita um registro de pedido
        /// 
        /// > POST: api/Pedido/5
        /// </summary>
        /// <param name="id">Id do registro a ser editado</param>
        /// <param name="pedido">Dados do registro a serem alterados, os dados não alterados também devem ser informados</param>
        /// <returns></returns>
        /// <response code="200">Registro editado</response>
        /// <response code="404">Registro não encontrado.</response>
        [HttpPost("{id}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult Edit(int id, [FromBody] PedidoDto pedido)
        {
            try
            {
                pedidoBusiness.Edit(id, pedido);
            }
            catch (System.Exception)
            {
                return NotFound();
            }

            return Ok();
        }

        /// <summary>
        /// Exclui um registro de pedido
        /// 
        /// > DELETE: api/Pedido/5
        /// </summary>
        /// <param name="id">Id do registro a ser excluído</param>
        /// <returns></returns>
        /// <response code="200">Registro excluído</response>
        /// <response code="404">Registro não encontrado.</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult Delete(int id)
        {
            try
            {
                pedidoBusiness.Delete(id);
            }
            catch (System.Exception)
            {
                return NotFound();
            }

            return Ok();
        }
    }
}
