﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SistemaAcaiBusiness.Interfaces;
using SistemaAcaiDomain.Dto;
using System.Collections.Generic;
using System.Linq;

namespace SistemaAcaiApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SaborController : ControllerBase
    {
        private readonly ISaborBusiness saborBusiness;

        public SaborController(ISaborBusiness saborBusiness)
        {
            this.saborBusiness = saborBusiness;
        }

        /// <summary>
        /// Consulta os registros de sabor
        /// 
        /// > GET: api/Sabor
        /// </summary>
        /// <returns>Lista de registros de sabores encontrados no banco de dados</returns>
        /// <response code="200">Registros encontrados</response>
        /// <response code="404">Nenhum registro encontrado.</response>
        [HttpGet]
        [ProducesResponseType(typeof(SaborDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult<IEnumerable<SaborDto>> Get()
        {
            var sabores = saborBusiness.Get();
            if (sabores == null || !sabores.Any())
            {
                return NotFound();
            }

            return new ObjectResult(sabores);
        }

        /// <summary>
        /// Consulta um registro de sabor por id
        /// 
        /// > GET: api/Sabor/5
        /// </summary>
        /// <param name="id">Id do registro a ser consultado</param>
        /// <returns>Registro de sabor do id consultado</returns>
        /// <response code="200">Registro encontrado</response>
        /// <response code="404">Registro não encontrado.</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(SaborDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult<SaborDto> Get(int id)
        {
            var sabor = saborBusiness.Get(id);
            if (sabor == null)
            {
                return NotFound();
            }

            return new ObjectResult(sabor);
        }

        /// <summary>
        /// Insere um registro de sabor
        /// 
        /// > POST: api/Sabor
        /// </summary>
        /// <param name="sabor">Dados do registro a ser inserido</param>
        /// <returns></returns>
        /// <response code="200">Registro inserido</response>
        [HttpPost]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        public ActionResult Post([FromBody] SaborDto sabor)
        {
            saborBusiness.Insert(sabor);
            return Ok();
        }

        /// <summary>
        /// Edita um registro de sabor
        /// 
        /// > POST: api/Sabor/5
        /// </summary>
        /// <param name="id">Id do registro a ser editado</param>
        /// <param name="sabor">Dados do registro a serem alterados, os dados não alterados também devem ser informados</param>
        /// <returns></returns>
        /// <response code="200">Registro editado</response>
        /// <response code="404">Registro não encontrado.</response>
        [HttpPost("{id}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult Edit(int id, [FromBody] SaborDto sabor)
        {
            try
            {
                saborBusiness.Edit(id, sabor);
            }
            catch (System.Exception)
            {
                return NotFound();
            }

            return Ok();
        }

        /// <summary>
        /// Exclui um registro de sabor
        /// 
        /// > DELETE: api/Sabor/5
        /// </summary>
        /// <param name="id">Id do registro a ser excluído</param>
        /// <returns></returns>
        /// <response code="200">Registro excluído</response>
        /// <response code="404">Registro não encontrado.</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult Delete(int id)
        {
            try
            {
                saborBusiness.Delete(id);
            }
            catch (System.Exception)
            {
                return NotFound();
            }

            return Ok();
        }
    }
}
