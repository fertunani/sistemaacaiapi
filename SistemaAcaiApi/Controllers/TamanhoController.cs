﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SistemaAcaiBusiness.Interfaces;
using SistemaAcaiDomain.Dto;
using System.Collections.Generic;
using System.Linq;

namespace SistemaAcaiApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TamanhoController : ControllerBase
    {
        private readonly ITamanhoBusiness tamanhoBusiness;

        public TamanhoController(ITamanhoBusiness tamanhoBusiness)
        {
            this.tamanhoBusiness = tamanhoBusiness;
        }

        /// <summary>
        /// Consulta os registros de tamanho
        /// 
        /// > GET: api/Tamanho
        /// </summary>
        /// <returns>Lista de registros de tamanhos encontrados no banco de dados</returns>
        /// <response code="200">Registros encontrados</response>
        /// <response code="404">Nenhum registro encontrado.</response>
        [HttpGet]
        [ProducesResponseType(typeof(TamanhoDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult<IEnumerable<TamanhoDto>> Get()
        {
            var tamanhos = tamanhoBusiness.Get();
            if (tamanhos == null || !tamanhos.Any())
            {
                return NotFound();
            }

            return new ObjectResult(tamanhos);
        }

        /// <summary>
        /// Consulta um registro de tamanho por id
        /// 
        /// > GET: api/Tamanho/5
        /// </summary>
        /// <param name="id">Id do registro a ser consultado</param>
        /// <returns>Registro de tamanho do id consultado</returns>
        /// <response code="200">Registro encontrado</response>
        /// <response code="404">Registro não encontrado.</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(TamanhoDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult<TamanhoDto> Get(int id)
        {
            var tamanho = tamanhoBusiness.Get(id);
            if (tamanho == null)
            {
                return NotFound();
            }

            return new ObjectResult(tamanho);
        }

        /// <summary>
        /// Insere um registro de tamanho
        /// 
        /// > POST: api/Tamanho
        /// </summary>
        /// <param name="tamanho">Dados do registro a ser inserido</param>
        /// <returns></returns>
        /// <response code="200">Registro inserido</response>
        [HttpPost]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        public ActionResult Post([FromBody] TamanhoDto tamanho)
        {
            tamanhoBusiness.Insert(tamanho);
            return Ok();
        }

        /// <summary>
        /// Edita um registro de tamanho
        /// 
        /// > POST: api/Tamanho/5
        /// </summary>
        /// <param name="id">Id do registro a ser editado</param>
        /// <param name="tamanho">Dados do registro a serem alterados, os dados não alterados também devem ser informados</param>
        /// <returns></returns>
        /// <response code="200">Registro editado</response>
        /// <response code="404">Registro não encontrado.</response>
        [HttpPost("{id}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult Edit(int id, [FromBody] TamanhoDto tamanho)
        {
            try
            {
                tamanhoBusiness.Edit(id, tamanho);
            }
            catch (System.Exception)
            {
                return NotFound();
            }

            return Ok();
        }

        /// <summary>
        /// Exclui um registro de tamanho
        /// 
        /// > DELETE: api/Tamanho/5
        /// </summary>
        /// <param name="id">Id do registro a ser excluído</param>
        /// <returns></returns>
        /// <response code="200">Registro excluído</response>
        /// <response code="404">Registro não encontrado.</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult Delete(int id)
        {
            try
            {
                tamanhoBusiness.Delete(id);
            }
            catch (System.Exception)
            {
                return NotFound();
            }

            return Ok();
        }
    }
}
