﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using SistemaAcaiBusiness.AutoMapper;
using SistemaAcaiBusiness.Business;
using SistemaAcaiBusiness.Interfaces;
using SistemaAcaiDomain.Data;
using SistemaAcaiRepository.Interfaces;
using SistemaAcaiRepository.Repository;
using System;
using System.IO;
using System.Reflection;

namespace SistemaAcaiApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureRepositories(services);
            ConfigureBusiness(services);
            ConfigureMapper(services);
            ConfigureSwagger(services);

            services.AddDbContext<SistemaAcaiContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        private void ConfigureSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Title = "Sistema Açaí API",
                        Version = "v1",
                        Description = "APIs do Sistema Açaí - Teste de desenvolvimento UDS",
                        Contact = new OpenApiContact
                        {
                            Name = "Murilo Fertunani",
                            Url = new Uri("https://gitlab.com/fertunani/sistemaacaiapi")
                        }
                    });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        private void ConfigureMapper(IServiceCollection services)
        {
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);
        }

        private void ConfigureBusiness(IServiceCollection services)
        {
            services.AddTransient<IAdicionalBusiness, AdicionalBusiness>();
            services.AddTransient<IPedidoAdicionalBusiness, PedidoAdicionalBusiness>();
            services.AddTransient<IPedidoBusiness, PedidoBusiness>();
            services.AddTransient<ISaborBusiness, SaborBusiness>();
            services.AddTransient<ITamanhoBusiness, TamanhoBusiness>();
        }

        private void ConfigureRepositories(IServiceCollection services)
        {
            services.AddTransient<IAdicionalRepository, AdicionalRepository>();
            services.AddTransient<IPedidoAdicionalRepository, PedidoAdicionalRepository>();
            services.AddTransient<IPedidoRepository, PedidoRepository>();
            services.AddTransient<ISaborRepository, SaborRepository>();
            services.AddTransient<ITamanhoRepository, TamanhoRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c => 
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Sistema Açaí API");
            });
        }
    }
}
