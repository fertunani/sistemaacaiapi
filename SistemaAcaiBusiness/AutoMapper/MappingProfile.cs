﻿using AutoMapper;
using SistemaAcaiDomain.Dto;
using SistemaAcaiDomain.Entities;

namespace SistemaAcaiBusiness.AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<AdicionalDto, Adicional>(MemberList.None)
                .ForMember(dest => dest.Id, o => o.Ignore());

            CreateMap<PedidoAdicionalDto, PedidoAdicional>(MemberList.None)
                .ForMember(dest => dest.Id, o => o.Ignore());

            CreateMap<PedidoDto, Pedido>(MemberList.None)
                .ForMember(dest => dest.Id, o => o.Ignore());

            CreateMap<SaborDto, Sabor>(MemberList.None)
                .ForMember(dest => dest.Id, o => o.Ignore());

            CreateMap<TamanhoDto, Tamanho>(MemberList.None)
                .ForMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<Adicional, AdicionalDto>(MemberList.None);

            CreateMap<PedidoAdicional, PedidoAdicionalDto>(MemberList.None);

            CreateMap<Pedido, PedidoDto>(MemberList.None);

            CreateMap<Sabor, SaborDto>(MemberList.None);

            CreateMap<Tamanho, TamanhoDto>(MemberList.None);
        }
    }
}
