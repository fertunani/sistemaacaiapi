﻿using AutoMapper;
using SistemaAcaiBusiness.Interfaces;
using SistemaAcaiDomain.Dto;
using SistemaAcaiDomain.Entities;
using SistemaAcaiRepository.Interfaces;
using System;
using System.Collections.Generic;

namespace SistemaAcaiBusiness.Business
{
    public class AdicionalBusiness : IAdicionalBusiness
    {
        private readonly IAdicionalRepository adicionalRepository;
        private readonly IMapper mapper;

        public AdicionalBusiness(IAdicionalRepository adicionalRepository, IMapper mapper)
        {
            this.adicionalRepository = adicionalRepository;
            this.mapper = mapper;
        }

        public void Delete(int id)
        {
            var entidade = adicionalRepository.Get(id);
            if (entidade == null)
            {
                throw new Exception();
            }

            adicionalRepository.Delete(entidade);
        }

        public void Edit(int id, AdicionalDto adicional)
        {
            var entidade = adicionalRepository.Get(id);
            if (entidade == null)
            {
                throw new Exception();
            }

            entidade.Descricao = adicional.Descricao;
            entidade.TempoPreparo = adicional.TempoPreparo ?? 0;
            entidade.Valor = adicional.Valor ?? 0;
            adicionalRepository.Edit(entidade);
        }

        public IEnumerable<AdicionalDto> Get()
        {
            var entidades = adicionalRepository.Get();
            return mapper.Map<IEnumerable<AdicionalDto>>(entidades);

        }

        public AdicionalDto Get(int id)
        {
            var entidade = adicionalRepository.Get(id);
            return mapper.Map<AdicionalDto>(entidade);
        }

        public void Insert(AdicionalDto adicional)
        {
            var entidade = new Adicional();
            entidade = mapper.Map<Adicional>(adicional);

            adicionalRepository.Insert(entidade);
        }
    }
}
