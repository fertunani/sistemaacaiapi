﻿using AutoMapper;
using SistemaAcaiBusiness.Interfaces;
using SistemaAcaiDomain.Dto;
using SistemaAcaiDomain.Entities;
using SistemaAcaiRepository.Interfaces;
using System;
using System.Collections.Generic;

namespace SistemaAcaiBusiness.Business
{
    public class PedidoAdicionalBusiness : IPedidoAdicionalBusiness
    {
        private readonly IPedidoAdicionalRepository pedidoAdicionalRepository;
        private readonly IPedidoRepository pedidoRepository;
        private readonly IAdicionalRepository adicionalRepository;
        private readonly IPedidoBusiness pedidoBusiness;
        private readonly IMapper mapper;

        public PedidoAdicionalBusiness(IPedidoAdicionalRepository pedidoAdicionalRepository,
            IPedidoRepository pedidoRepository,
            IAdicionalRepository adicionalRepository,
            IPedidoBusiness pedidoBusiness,
            IMapper mapper)
        {
            this.pedidoAdicionalRepository = pedidoAdicionalRepository;
            this.pedidoRepository = pedidoRepository;
            this.adicionalRepository = adicionalRepository;
            this.pedidoBusiness = pedidoBusiness;
            this.mapper = mapper;
        }

        public void Delete(int id)
        {
            var entidade = pedidoAdicionalRepository.Get(id);
            if (entidade == null)
            {
                throw new Exception();
            }

            var pedidoAdicionalDto = new PedidoAdicionalDto();
            pedidoAdicionalDto = mapper.Map<PedidoAdicionalDto>(entidade);
            ProcessarPedidoAdicional(pedidoAdicionalDto, true);

            pedidoAdicionalRepository.Delete(entidade);
        }

        public void Edit(int id, PedidoAdicionalDto pedidoAdicional)
        {
            var entidade = pedidoAdicionalRepository.Get(id);
            if (entidade == null)
            {
                throw new Exception();
            }

            ProcessarPedidoAdicionalEdit(entidade, pedidoAdicional);
            entidade.AdicionalId = pedidoAdicional.AdicionalId;
            entidade.PedidoId = pedidoAdicional.PedidoId;
            pedidoAdicionalRepository.Edit(entidade);
        }

        public IEnumerable<PedidoAdicionalDto> Get()
        {
            var entidades = pedidoAdicionalRepository.Get();
            return mapper.Map<IEnumerable<PedidoAdicionalDto>>(entidades);
        }

        public PedidoAdicionalDto Get(int id)
        {
            var entidade = pedidoAdicionalRepository.Get(id);
            return mapper.Map<PedidoAdicionalDto>(entidade);
        }

        public int Insert(PedidoAdicionalDto pedidoAdicional)
        {
            ProcessarPedidoAdicional(pedidoAdicional);
            var entidade = new PedidoAdicional();
            entidade = mapper.Map<PedidoAdicional>(pedidoAdicional);

            return pedidoAdicionalRepository.Insert(entidade);
        }

        #region Private Methods
        private void ProcessarPedidoAdicional(PedidoAdicionalDto pedidoAdicional, bool exclusao = false)
        {
            var adicional = adicionalRepository.Get(pedidoAdicional.AdicionalId);
            var pedido = pedidoRepository.Get(pedidoAdicional.PedidoId);
            var pedidoDto = new PedidoDto();
            pedidoDto = mapper.Map<PedidoDto>(pedido);

            if (!exclusao)
            {
                pedidoDto.TempoPreparo += adicional.TempoPreparo ?? 0;
                pedidoDto.ValorTotal += adicional.Valor ?? 0;
            }
            else
            {
                pedidoDto.TempoPreparo -= adicional.TempoPreparo ?? 0;
                pedidoDto.ValorTotal -= adicional.Valor ?? 0;
            }

            pedidoBusiness.EditByAdicional(pedido, pedidoDto);
        }

        private void ProcessarPedidoAdicionalEdit(PedidoAdicional entidade, PedidoAdicionalDto pedidoAdicional)
        {
            var pedidoAdicionalDto = new PedidoAdicionalDto();
            pedidoAdicionalDto = mapper.Map<PedidoAdicionalDto>(entidade);
            ProcessarPedidoAdicional(pedidoAdicionalDto, true);
            ProcessarPedidoAdicional(pedidoAdicional);
        }
        #endregion
    }
}
