﻿using AutoMapper;
using SistemaAcaiBusiness.Interfaces;
using SistemaAcaiDomain.Dto;
using SistemaAcaiDomain.Entities;
using SistemaAcaiDomain.Entities.Projections;
using SistemaAcaiRepository.Interfaces;
using System;
using System.Collections.Generic;

namespace SistemaAcaiBusiness.Business
{
    public class PedidoBusiness : IPedidoBusiness
    {
        private readonly IPedidoRepository pedidoRepository;
        private readonly ITamanhoRepository tamanhoRepository;
        private readonly ISaborRepository saborRepository;
        private readonly IAdicionalRepository adicionalRepository;
        private readonly IPedidoAdicionalRepository pedidoAdicionalRepository;
        private readonly IMapper mapper;

        public PedidoBusiness(IPedidoRepository pedidoRepository,
            ITamanhoRepository tamanhoRepository,
            ISaborRepository saborRepository,
            IAdicionalRepository adicionalRepository,
            IPedidoAdicionalRepository pedidoAdicionalRepository,
            IMapper mapper)
        {
            this.pedidoRepository = pedidoRepository;
            this.tamanhoRepository = tamanhoRepository;
            this.saborRepository = saborRepository;
            this.adicionalRepository = adicionalRepository;
            this.pedidoAdicionalRepository = pedidoAdicionalRepository;
            this.mapper = mapper;
        }

        public void Delete(int id)
        {
            var entidade = pedidoRepository.Get(id);
            if (entidade == null)
            {
                throw new Exception();
            }

            pedidoRepository.Delete(entidade);
        }

        public void Edit(int id, PedidoDto pedido)
        {
            var entidade = pedidoRepository.Get(id);
            if (entidade == null)
            {
                throw new Exception();
            }

            ProcessarPedido(pedido);
            entidade.SaborId = pedido.SaborId;
            entidade.TamanhoId = pedido.TamanhoId;
            entidade.TempoPreparo = pedido.TempoPreparo;
            entidade.ValorTotal = pedido.ValorTotal;
            pedidoRepository.Edit(entidade);
        }

        public void EditByAdicional(Pedido entidade, PedidoDto pedido)
        {
            entidade.TempoPreparo = pedido.TempoPreparo;
            entidade.ValorTotal = pedido.ValorTotal;
            pedidoRepository.Edit(entidade);
        }

        public IEnumerable<PedidoInfo> Get()
        {
            var info = new List<PedidoInfo>();
            var entidades = pedidoRepository.Get();
            var pedidos = mapper.Map<IEnumerable<PedidoDto>>(entidades);

            foreach (var pedido in pedidos)
            {
                info.Add(Get(pedido.Id));
            }

            return info;
        }

        public PedidoInfo Get(int id)
        {
            var info = GerarPedidoInfo(id);
            GerarAdicionais(id, info);

            return info;
        }

        public int Insert(PedidoDto pedido)
        {
            ProcessarPedido(pedido);
            var entidade = new Pedido();
            entidade = mapper.Map<Pedido>(pedido);

            return pedidoRepository.Insert(entidade);
        }


        #region Private Methods
        private void ProcessarPedido(PedidoDto pedido)
        {
            var tamanho = tamanhoRepository.Get(pedido.TamanhoId);
            var sabor = saborRepository.Get(pedido.SaborId);
            pedido.TempoPreparo = tamanho.TempoPreparo + (sabor.TempoPreparo ?? 0);
            pedido.ValorTotal = tamanho.Valor;
        }

        private void GerarAdicionais(int id, PedidoInfo info)
        {
            foreach (var pedidoAdicional in pedidoAdicionalRepository.GetByPedido(id))
            {
                var adicional = adicionalRepository.Get(pedidoAdicional.AdicionalId);
                info.Adicionais.Add(new AdicionalInfo { Adicional = adicional.Descricao, Valor = adicional.Valor });
            }
        }

        private PedidoInfo GerarPedidoInfo(int id)
        {
            var entidade = pedidoRepository.Get(id);
            if (entidade == null)
            {
                return null;
            }

            var pedido = mapper.Map<PedidoDto>(entidade);
            var tamanho = tamanhoRepository.Get(pedido.TamanhoId);
            var sabor = saborRepository.Get(pedido.SaborId);

            var info = new PedidoInfo
            {
                Id = id,
                Tamanho = tamanho.Descricao,
                Sabor = sabor.Descricao,
                Valor = tamanho.Valor,
                Adicionais = new List<AdicionalInfo>(),
                TempoPreparo = pedido.TempoPreparo,
                ValorTotal = pedido.ValorTotal
            };

            return info;
        }
        #endregion
    }
}
