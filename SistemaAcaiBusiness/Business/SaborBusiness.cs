﻿using AutoMapper;
using SistemaAcaiBusiness.Interfaces;
using SistemaAcaiDomain.Dto;
using SistemaAcaiDomain.Entities;
using SistemaAcaiRepository.Interfaces;
using System;
using System.Collections.Generic;

namespace SistemaAcaiBusiness.Business
{
    public class SaborBusiness : ISaborBusiness
    {
        private readonly ISaborRepository saborRepository;
        private readonly IMapper mapper;

        public SaborBusiness(ISaborRepository saborRepository, IMapper mapper)
        {
            this.saborRepository = saborRepository;
            this.mapper = mapper;
        }

        public void Delete(int id)
        {
            var entidade = saborRepository.Get(id);
            if (entidade == null)
            {
                throw new Exception();
            }

            saborRepository.Delete(entidade);
        }

        public void Edit(int id, SaborDto sabor)
        {
            var entidade = saborRepository.Get(id);
            if (entidade == null)
            {
                throw new Exception();
            }

            entidade.Descricao = sabor.Descricao;
            entidade.TempoPreparo = sabor.TempoPreparo;
            saborRepository.Edit(entidade);
        }

        public IEnumerable<SaborDto> Get()
        {
            var entidades = saborRepository.Get();
            return mapper.Map<IEnumerable<SaborDto>>(entidades);
        }

        public SaborDto Get(int id)
        {
            var entidade = saborRepository.Get(id);
            return mapper.Map<SaborDto>(entidade);
        }

        public void Insert(SaborDto sabor)
        {
            var entidade = new Sabor();
            entidade = mapper.Map<Sabor>(sabor);

            saborRepository.Insert(entidade);
        }
    }
}
