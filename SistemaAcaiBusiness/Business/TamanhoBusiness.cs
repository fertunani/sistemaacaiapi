﻿using AutoMapper;
using SistemaAcaiBusiness.Interfaces;
using SistemaAcaiDomain.Dto;
using SistemaAcaiDomain.Entities;
using SistemaAcaiRepository.Interfaces;
using System;
using System.Collections.Generic;

namespace SistemaAcaiBusiness.Business
{
    public class TamanhoBusiness : ITamanhoBusiness
    {
        private readonly ITamanhoRepository tamanhoRepository;
        private readonly IMapper mapper;

        public TamanhoBusiness(ITamanhoRepository tamanhoRepository, IMapper mapper)
        {
            this.tamanhoRepository = tamanhoRepository;
            this.mapper = mapper;
        }

        public void Delete(int id)
        {
            var entidade = tamanhoRepository.Get(id);
            if (entidade == null)
            {
                throw new Exception();
            }

            tamanhoRepository.Delete(entidade);
        }

        public void Edit(int id, TamanhoDto tamanho)
        {
            var entidade = tamanhoRepository.Get(id);
            if (entidade == null)
            {
                throw new Exception();
            }

            entidade.Descricao = tamanho.Descricao;
            entidade.TempoPreparo = tamanho.TempoPreparo;
            entidade.Valor = tamanho.Valor;
            tamanhoRepository.Edit(entidade);
        }

        public IEnumerable<TamanhoDto> Get()
        {
            var entidades = tamanhoRepository.Get();
            return mapper.Map<IEnumerable<TamanhoDto>>(entidades);
        }

        public TamanhoDto Get(int id)
        {
            var entidade = tamanhoRepository.Get(id);
            return mapper.Map<TamanhoDto>(entidade);
        }

        public void Insert(TamanhoDto tamanho)
        {
            var entidade = new Tamanho();
            entidade = mapper.Map<Tamanho>(tamanho);

            tamanhoRepository.Insert(entidade);
        }
    }
}
