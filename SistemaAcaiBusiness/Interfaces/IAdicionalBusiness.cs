﻿using SistemaAcaiDomain.Dto;
using System.Collections.Generic;

namespace SistemaAcaiBusiness.Interfaces
{
    public interface IAdicionalBusiness
    {
        IEnumerable<AdicionalDto> Get();
        AdicionalDto Get(int id);
        void Edit(int id, AdicionalDto adicional);
        void Insert(AdicionalDto adicional);
        void Delete(int id);
    }
}
