﻿using SistemaAcaiDomain.Dto;
using System.Collections.Generic;

namespace SistemaAcaiBusiness.Interfaces
{
    public interface IPedidoAdicionalBusiness
    {
        IEnumerable<PedidoAdicionalDto> Get();
        PedidoAdicionalDto Get(int id);
        void Edit(int id, PedidoAdicionalDto pedidoAdicional);
        int Insert(PedidoAdicionalDto pedidoAdicional);
        void Delete(int id);
    }
}
