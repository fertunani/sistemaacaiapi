﻿using SistemaAcaiDomain.Dto;
using SistemaAcaiDomain.Entities;
using SistemaAcaiDomain.Entities.Projections;
using System.Collections.Generic;

namespace SistemaAcaiBusiness.Interfaces
{
    public interface IPedidoBusiness
    {
        IEnumerable<PedidoInfo> Get();
        PedidoInfo Get(int id);
        void Edit(int id, PedidoDto pedido);
        int Insert(PedidoDto pedido);
        void Delete(int id);
        void EditByAdicional(Pedido entidade, PedidoDto pedido);
    }
}
