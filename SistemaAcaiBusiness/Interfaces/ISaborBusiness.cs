﻿using SistemaAcaiDomain.Dto;
using System.Collections.Generic;

namespace SistemaAcaiBusiness.Interfaces
{
    public interface ISaborBusiness
    {
        IEnumerable<SaborDto> Get();
        SaborDto Get(int id);
        void Edit(int id, SaborDto sabor);
        void Insert(SaborDto sabor);
        void Delete(int id);
    }
}
