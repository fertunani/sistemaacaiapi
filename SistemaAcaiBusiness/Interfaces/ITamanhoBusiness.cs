﻿using SistemaAcaiDomain.Dto;
using System.Collections.Generic;

namespace SistemaAcaiBusiness.Interfaces
{
    public interface ITamanhoBusiness
    {
        IEnumerable<TamanhoDto> Get();
        TamanhoDto Get(int id);
        void Edit(int id, TamanhoDto tamanho);
        void Insert(TamanhoDto tamanho);
        void Delete(int id);
    }
}
