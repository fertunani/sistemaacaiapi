﻿using AutoMapper;
using Moq;
using NUnit.Framework;
using SistemaAcaiBusiness.AutoMapper;
using SistemaAcaiBusiness.Business;
using SistemaAcaiBusiness.Interfaces;
using SistemaAcaiDomain.Dto;
using SistemaAcaiDomain.Entities;
using SistemaAcaiRepository.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace SistemaAcaiBusinessTest.Business
{
    [TestFixture]
    public class AdicionalBusinessTest
    {
        private Mock<IAdicionalRepository> adicionalRepository;
        private IAdicionalBusiness adicionalBusiness;
        private IMapper mapper;

        [SetUp]
        public void SetUp()
        {
            adicionalRepository = new Mock<IAdicionalRepository>();
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            mapper = mapperConfig.CreateMapper();

            adicionalBusiness = new AdicionalBusiness(adicionalRepository.Object, mapper);
        }

        [Test]
        public void DeveRetornarUmaInstanciaDeAdicional()
        {
            var entidade = new Adicional
            {
                Id = 1,
                Descricao = "paçoca",
                TempoPreparo = 3,
                Valor = 3
            };

            adicionalRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(entidade);
            var adicional = adicionalBusiness.Get(entidade.Id);

            Assert.AreEqual(entidade.Id, adicional.Id);
            Assert.AreEqual(entidade.Descricao, adicional.Descricao);
            Assert.AreEqual(entidade.TempoPreparo, adicional.TempoPreparo);

            adicionalRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            adicionalRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveRetornarVariasIntanciasDeAdicional()
        {
            var entidades = new List<Adicional>
            {
                new Adicional
                {
                    Id = 1,
                    Descricao = "paçoca",
                    TempoPreparo = 3,
                    Valor = 3
                },
                new Adicional
                {
                    Id = 2,
                    Descricao = "leite ninho",
                    TempoPreparo = 2,
                    Valor = 3
                }
            };

            adicionalRepository.Setup(s => s.Get()).Returns(entidades);
            var adicionais = adicionalBusiness.Get();

            Assert.AreEqual(entidades.Count(), adicionais.Count());

            adicionalRepository.Verify(v => v.Get(), Times.Once());
            adicionalRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveExcluirUmaInstanciaDeAdicional()
        {
            var entidade = new Adicional
            {
                Id = 1,
                Descricao = "paçoca",
                TempoPreparo = 3,
                Valor = 3
            };

            adicionalRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(entidade);
            adicionalRepository.Setup(s => s.Delete(It.IsAny<Adicional>()));

            adicionalBusiness.Delete(entidade.Id);
            adicionalRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            adicionalRepository.Verify(v => v.Delete(It.IsAny<Adicional>()), Times.Once());
            adicionalRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveEditarUmaInstanciaDeAdicional()
        {
            var entidade = new Adicional
            {
                Id = 1,
                Descricao = "paçoca",
                TempoPreparo = 3,
                Valor = 3
            };

            var dto = new AdicionalDto
            {
                Descricao = "paçoca",
                TempoPreparo = 4,
                Valor = 3
            };

            adicionalRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(entidade);
            adicionalRepository.Setup(s => s.Edit(It.IsAny<Adicional>()));

            adicionalBusiness.Edit(entidade.Id, dto);
            adicionalRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            adicionalRepository.Verify(v => v.Edit(It.IsAny<Adicional>()), Times.Once());
            adicionalRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveInserirUmaInstanciaDeAdicional()
        {
            var dto = new AdicionalDto
            {
                Descricao = "paçoca",
                TempoPreparo = 4,
                Valor = 3
            };

            adicionalRepository.Setup(s => s.Insert(It.IsAny<Adicional>()));

            adicionalBusiness.Insert(dto);

            adicionalRepository.Verify(v => v.Insert(It.IsAny<Adicional>()), Times.Once());
            adicionalRepository.VerifyNoOtherCalls();
        }
    }
}