﻿using AutoMapper;
using Moq;
using NUnit.Framework;
using SistemaAcaiBusiness.AutoMapper;
using SistemaAcaiBusiness.Business;
using SistemaAcaiBusiness.Interfaces;
using SistemaAcaiDomain.Dto;
using SistemaAcaiDomain.Entities;
using SistemaAcaiRepository.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace SistemaAcaiBusinessTest.Business
{
    [TestFixture]
    public class PedidoAdicionalBusinessTest
    {
        private Mock<IPedidoAdicionalRepository> pedidoAdicionalRepository;
        private IPedidoAdicionalBusiness pedidoAdicionalBusiness;
        private Mock<IPedidoRepository> pedidoRepository;
        private Mock<ITamanhoRepository> tamanhoRepository;
        private Mock<ISaborRepository> saborRepository;
        private Mock<IAdicionalRepository> adicionalRepository;
        private IPedidoBusiness pedidoBusiness;
        private IMapper mapper;

        [SetUp]
        public void SetUp()
        {
            pedidoAdicionalRepository = new Mock<IPedidoAdicionalRepository>();
            pedidoRepository = new Mock<IPedidoRepository>();
            tamanhoRepository = new Mock<ITamanhoRepository>();
            saborRepository = new Mock<ISaborRepository>();
            adicionalRepository = new Mock<IAdicionalRepository>();
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            mapper = mapperConfig.CreateMapper();

            pedidoBusiness = new PedidoBusiness(pedidoRepository.Object,
                tamanhoRepository.Object,
                saborRepository.Object,
                adicionalRepository.Object,
                pedidoAdicionalRepository.Object,
                mapper);

            pedidoAdicionalBusiness = new PedidoAdicionalBusiness(pedidoAdicionalRepository.Object,
                pedidoRepository.Object,
                adicionalRepository.Object,
                pedidoBusiness,
                mapper);
        }

        [Test]
        public void DeveRetornarUmaInstanciaDePedidoAdicional()
        {
            var entidade = new PedidoAdicional
            {
                Id = 1,
                PedidoId = 1,
                AdicionalId = 1
            };

            pedidoAdicionalRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(entidade);
            var pedidoAdicional = pedidoAdicionalBusiness.Get(entidade.Id);

            Assert.AreEqual(entidade.Id, pedidoAdicional.Id);
            Assert.AreEqual(entidade.PedidoId, pedidoAdicional.PedidoId);
            Assert.AreEqual(entidade.AdicionalId, pedidoAdicional.AdicionalId);

            pedidoAdicionalRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            pedidoAdicionalRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveRetornarVariasIntanciasDePedidoAdicional()
        {
            var entidades = new List<PedidoAdicional>
            {
                new PedidoAdicional
                {
                    Id = 1,
                    PedidoId = 1,
                    AdicionalId = 1
                },
                new PedidoAdicional
                {
                    Id = 2,
                    PedidoId = 2,
                    AdicionalId = 2
                }
            };

            pedidoAdicionalRepository.Setup(s => s.Get()).Returns(entidades);
            var pedidosAdicionais = pedidoAdicionalBusiness.Get();

            Assert.AreEqual(entidades.Count(), pedidosAdicionais.Count());

            pedidoAdicionalRepository.Verify(v => v.Get(), Times.Once());
            pedidoAdicionalRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveExcluirUmaInstanciaDePedidoAdicional()
        {
            var entidade = new PedidoAdicional
            {
                Id = 1,
                PedidoId = 1,
                AdicionalId = 1
            };

            var pedido = new Pedido
            {
                Id = 1,
                TamanhoId = 1,
                SaborId = 1,
                TempoPreparo = 13,
                ValorTotal = 13
            };

            var adicional = new Adicional
            {
                Id = 2,
                Descricao = "paçoca",
                TempoPreparo = 3,
                Valor = 3
            };

            pedidoAdicionalRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(entidade);
            adicionalRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(adicional);
            pedidoRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(pedido);
            pedidoRepository.Setup(s => s.Edit(It.IsAny<Pedido>()));
            pedidoAdicionalRepository.Setup(s => s.Delete(It.IsAny<PedidoAdicional>()));

            pedidoAdicionalBusiness.Delete(entidade.Id);

            pedidoAdicionalRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            adicionalRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            pedidoRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            pedidoRepository.Verify(v => v.Edit(It.IsAny<Pedido>()), Times.Once());
            pedidoAdicionalRepository.Verify(v => v.Delete(It.IsAny<PedidoAdicional>()), Times.Once());

            pedidoAdicionalRepository.VerifyNoOtherCalls();
            adicionalRepository.VerifyNoOtherCalls();
            pedidoRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveEditarUmaInstanciaDePedidoAdicional()
        {
            var entidade = new PedidoAdicional
            {
                Id = 1,
                PedidoId = 1,
                AdicionalId = 1
            };

            var dto = new PedidoAdicionalDto
            {
                PedidoId = 1,
                AdicionalId = 2
            };

            var pedido = new Pedido
            {
                Id = 1,
                TamanhoId = 1,
                SaborId = 1,
                TempoPreparo = 13,
                ValorTotal = 13
            };

            var adicional = new Adicional
            {
                Id = 2,
                Descricao = "paçoca",
                TempoPreparo = 3,
                Valor = 3
            };

            pedidoAdicionalRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(entidade);
            adicionalRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(adicional);
            pedidoRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(pedido);
            pedidoRepository.Setup(s => s.Edit(It.IsAny<Pedido>()));
            pedidoAdicionalRepository.Setup(s => s.Edit(It.IsAny<PedidoAdicional>()));

            pedidoAdicionalBusiness.Edit(entidade.Id, dto);

            pedidoAdicionalRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            adicionalRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Exactly(2));
            pedidoRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Exactly(2));
            pedidoRepository.Verify(v => v.Edit(It.IsAny<Pedido>()), Times.Exactly(2));
            pedidoAdicionalRepository.Verify(v => v.Edit(It.IsAny<PedidoAdicional>()), Times.Once());

            pedidoAdicionalRepository.VerifyNoOtherCalls();
            adicionalRepository.VerifyNoOtherCalls();
            pedidoRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveInserirUmaInstanciaDePedidoAdicional()
        {
            var dto = new PedidoAdicionalDto
            {
                PedidoId = 1,
                AdicionalId = 2
            };

            var pedido = new Pedido
            {
                Id = 1,
                TamanhoId = 1,
                SaborId = 1,
                TempoPreparo = 13,
                ValorTotal = 13
            };

            var adicional = new Adicional
            {
                Id = 2,
                Descricao = "paçoca",
                TempoPreparo = 3,
                Valor = 3
            };

            adicionalRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(adicional);
            pedidoRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(pedido);
            pedidoAdicionalRepository.Setup(s => s.Insert(It.IsAny<PedidoAdicional>()));

            pedidoAdicionalBusiness.Insert(dto);

            adicionalRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            pedidoRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            pedidoRepository.Verify(v => v.Edit(It.IsAny<Pedido>()), Times.Once());
            pedidoAdicionalRepository.Verify(v => v.Insert(It.IsAny<PedidoAdicional>()), Times.Once());

            pedidoAdicionalRepository.VerifyNoOtherCalls();
            adicionalRepository.VerifyNoOtherCalls();
            pedidoRepository.VerifyNoOtherCalls();
        }
    }
}
