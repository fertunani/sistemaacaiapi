﻿using AutoMapper;
using Moq;
using NUnit.Framework;
using SistemaAcaiBusiness.AutoMapper;
using SistemaAcaiBusiness.Business;
using SistemaAcaiBusiness.Interfaces;
using SistemaAcaiDomain.Dto;
using SistemaAcaiDomain.Entities;
using SistemaAcaiRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SistemaAcaiBusinessTest.Business
{
    [TestFixture]
    public class PedidoBusinessTest
    {
        private Mock<IPedidoRepository> pedidoRepository;
        private Mock<ITamanhoRepository> tamanhoRepository;
        private Mock<ISaborRepository> saborRepository;
        private Mock<IAdicionalRepository> adicionalRepository;
        private Mock<IPedidoAdicionalRepository> pedidoAdicionalRepository;

        private IPedidoBusiness pedidoBusiness;
        private IMapper mapper;

        [SetUp]
        public void SetUp()
        {
            pedidoRepository = new Mock<IPedidoRepository>();
            tamanhoRepository = new Mock<ITamanhoRepository>();
            saborRepository = new Mock<ISaborRepository>();
            adicionalRepository = new Mock<IAdicionalRepository>();
            pedidoAdicionalRepository = new Mock<IPedidoAdicionalRepository>();

            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            mapper = mapperConfig.CreateMapper();

            pedidoBusiness = new PedidoBusiness(pedidoRepository.Object,
                tamanhoRepository.Object,
                saborRepository.Object,
                adicionalRepository.Object,
                pedidoAdicionalRepository.Object,
                mapper);
        }

        [Test]
        public void DeveRetornarUmaInstanciaDePedido()
        {
            var objPedido = new Pedido
            {
                Id = 1,
                TamanhoId = 1,
                SaborId = 1,
                TempoPreparo = 13,
                ValorTotal = 13
            };

            var objPedidoAdicionais = new List<PedidoAdicional>
            {
                new PedidoAdicional
                {
                    Id = 1,
                    PedidoId = 1,
                    AdicionalId = 1
                },
                new PedidoAdicional
                {
                    Id = 2,
                    PedidoId = 1,
                    AdicionalId = 2
                }
            };

            var objTamanho = new Tamanho
            {
                Id = 1,
                Descricao = "pequeno (300ml)",
                TempoPreparo = 5,
                Valor = 7
            };

            var objSabor = new Sabor
            {
                Id = 1,
                Descricao = "morango",
                TempoPreparo = 3
            };

            var objAdicionais = new List<Adicional>
            {
                new Adicional
                {
                    Id = 1,
                    Descricao = "paçoca",
                    TempoPreparo = 3,
                    Valor = 3
                },
                new Adicional
                {
                    Id = 2,
                    Descricao = "leite ninho",
                    TempoPreparo = 2,
                    Valor = 3
                }
            };

            objPedido.ValorTotal = objTamanho.Valor + objAdicionais.Sum(s => s.Valor ?? 0);
            objPedido.TempoPreparo = objTamanho.TempoPreparo + objSabor.TempoPreparo ?? 0 + objAdicionais.Sum(s => s.TempoPreparo ?? 0);

            pedidoRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(objPedido);
            pedidoAdicionalRepository.Setup(s => s.GetByPedido(It.IsAny<int>())).Returns(objPedidoAdicionais);
            tamanhoRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(objTamanho);
            saborRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(objSabor);
            for (int i = 0; i < objAdicionais.Count; i++)
            {
                adicionalRepository.Setup(s => s.Get(objAdicionais[i].Id)).Returns(objAdicionais[i]);
            }

            var pedido = pedidoBusiness.Get(objPedido.Id);

            Assert.AreEqual(objPedido.Id, pedido.Id);
            Assert.AreEqual(objTamanho.Descricao, pedido.Tamanho);
            Assert.AreEqual(objSabor.Descricao, pedido.Sabor);
            Assert.AreEqual(objTamanho.Valor, pedido.Valor);
            Assert.AreEqual(objPedido.ValorTotal, pedido.ValorTotal);
            Assert.AreEqual(objPedido.TempoPreparo, pedido.TempoPreparo);
            for (int i = 0; i < objAdicionais.Count; i++)
            {
                Assert.AreEqual(objAdicionais[i].Descricao, pedido.Adicionais[i].Adicional);
                Assert.AreEqual(objAdicionais[i].Valor, pedido.Adicionais[i].Valor);
            }

            pedidoRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            tamanhoRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            saborRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            pedidoAdicionalRepository.Verify(v => v.GetByPedido(It.IsAny<int>()), Times.Once());
            adicionalRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Exactly(objAdicionais.Count));

            pedidoRepository.VerifyNoOtherCalls();
            tamanhoRepository.VerifyNoOtherCalls();
            saborRepository.VerifyNoOtherCalls();
            pedidoAdicionalRepository.VerifyNoOtherCalls();
            adicionalRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveRetornarVariasIntanciasDePedido()
        {
            var objPedidos = new List<Pedido>
            {
                new Pedido
                {
                    Id = 1,
                    TamanhoId = 1,
                    SaborId = 1,
                    TempoPreparo = 13,
                    ValorTotal = 13
                },
                new Pedido
                {
                    Id = 2,
                    TamanhoId = 1,
                    SaborId = 1,
                    TempoPreparo = 13,
                    ValorTotal = 13
                }
            };

            var objPedidoAdicionais = new List<PedidoAdicional>
            {
                new PedidoAdicional
                {
                    Id = 1,
                    PedidoId = 1,
                    AdicionalId = 1
                },
                new PedidoAdicional
                {
                    Id = 2,
                    PedidoId = 1,
                    AdicionalId = 2
                }
            };

            var objTamanho = new Tamanho
            {
                Id = 1,
                Descricao = "pequeno (300ml)",
                TempoPreparo = 5,
                Valor = 7
            };

            var objSabor = new Sabor
            {
                Id = 1,
                Descricao = "morango",
                TempoPreparo = 3
            };

            var objAdicionais = new List<Adicional>
            {
                new Adicional
                {
                    Id = 1,
                    Descricao = "paçoca",
                    TempoPreparo = 3,
                    Valor = 3
                },
                new Adicional
                {
                    Id = 2,
                    Descricao = "leite ninho",
                    TempoPreparo = 2,
                    Valor = 3
                }
            };

            pedidoRepository.Setup(s => s.Get()).Returns(objPedidos);
            for (int i = 0; i < objPedidos.Count; i++)
            {
                pedidoRepository.Setup(s => s.Get(objPedidos[i].Id)).Returns(objPedidos[i]);
            }
            pedidoAdicionalRepository.Setup(s => s.GetByPedido(It.IsAny<int>())).Returns(objPedidoAdicionais);
            tamanhoRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(objTamanho);
            saborRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(objSabor);
            for (int i = 0; i < objAdicionais.Count; i++)
            {
                adicionalRepository.Setup(s => s.Get(objAdicionais[i].Id)).Returns(objAdicionais[i]);
            }

            var pedido = pedidoBusiness.Get();

            pedidoRepository.Verify(v => v.Get(), Times.Once());
            pedidoRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Exactly(objPedidos.Count));
            tamanhoRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Exactly(objPedidos.Count));
            saborRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Exactly(objPedidos.Count));
            pedidoAdicionalRepository.Verify(v => v.GetByPedido(It.IsAny<int>()), Times.Exactly(objPedidos.Count));
            adicionalRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Exactly(objAdicionais.Count * objPedidos.Count));

            pedidoRepository.VerifyNoOtherCalls();
            tamanhoRepository.VerifyNoOtherCalls();
            saborRepository.VerifyNoOtherCalls();
            pedidoAdicionalRepository.VerifyNoOtherCalls();
            adicionalRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveExcluirUmaInstanciaDePedido()
        {
            var entidade = new Pedido
            {
                Id = 1,
                TamanhoId = 1,
                SaborId = 1,
                TempoPreparo = 13,
                ValorTotal = 13
            };

            pedidoRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(entidade);
            pedidoRepository.Setup(s => s.Delete(It.IsAny<Pedido>()));

            pedidoBusiness.Delete(entidade.Id);

            pedidoRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            pedidoRepository.Verify(v => v.Delete(It.IsAny<Pedido>()), Times.Once());
            pedidoRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveEditarUmaInstanciaDePedido()
        {
            var entidade = new Pedido
            {
                Id = 1,
                TamanhoId = 1,
                SaborId = 1,
                TempoPreparo = 13,
                ValorTotal = 13
            };

            var dto = new PedidoDto
            {
                TamanhoId = 2,
                SaborId = 2,
            };

            var objTamanho = new Tamanho
            {
                Id = 2,
                Descricao = "pequeno (300ml)",
                TempoPreparo = 5,
                Valor = 7
            };

            var objSabor = new Sabor
            {
                Id = 2,
                Descricao = "morango",
                TempoPreparo = 3
            };

            pedidoRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(entidade);
            pedidoRepository.Setup(s => s.Edit(It.IsAny<Pedido>()));
            tamanhoRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(objTamanho);
            saborRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(objSabor);

            pedidoBusiness.Edit(entidade.Id, dto);

            pedidoRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            pedidoRepository.Verify(v => v.Edit(It.IsAny<Pedido>()), Times.Once());
            tamanhoRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            saborRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());

            pedidoRepository.VerifyNoOtherCalls();
            tamanhoRepository.VerifyNoOtherCalls();
            saborRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveEditarUmaInstanciaDePedidoPorAdicional()
        {
            var entidade = new Pedido
            {
                Id = 1,
                TamanhoId = 1,
                SaborId = 1,
                TempoPreparo = 13,
                ValorTotal = 13
            };

            var dto = new PedidoDto
            {
                Id = 1,
                TamanhoId = 1,
                SaborId = 1,
                TempoPreparo = 15,
                ValorTotal = 15
            };

            
            pedidoRepository.Setup(s => s.Edit(It.IsAny<Pedido>()));

            pedidoBusiness.EditByAdicional(entidade, dto);

            pedidoRepository.Verify(v => v.Edit(It.IsAny<Pedido>()), Times.Once());

            pedidoRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveInserirUmaInstanciaDePedido()
        {
            var dto = new PedidoDto
            {
                TamanhoId = 2,
                SaborId = 2,
                TempoPreparo = 15,
                ValorTotal = 15
            };

            var objTamanho = new Tamanho
            {
                Id = 1,
                Descricao = "pequeno (300ml)",
                TempoPreparo = 5,
                Valor = 7
            };

            var objSabor = new Sabor
            {
                Id = 1,
                Descricao = "morango",
                TempoPreparo = 3
            };

            pedidoRepository.Setup(s => s.Insert(It.IsAny<Pedido>()));
            tamanhoRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(objTamanho);
            saborRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(objSabor);

            pedidoBusiness.Insert(dto);

            pedidoRepository.Verify(v => v.Insert(It.IsAny<Pedido>()), Times.Once());
            tamanhoRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            saborRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());

            pedidoRepository.VerifyNoOtherCalls();
            tamanhoRepository.VerifyNoOtherCalls();
            saborRepository.VerifyNoOtherCalls();
        }
    }
}
