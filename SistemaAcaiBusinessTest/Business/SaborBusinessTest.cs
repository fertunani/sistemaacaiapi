﻿using AutoMapper;
using Moq;
using NUnit.Framework;
using SistemaAcaiBusiness.AutoMapper;
using SistemaAcaiBusiness.Business;
using SistemaAcaiBusiness.Interfaces;
using SistemaAcaiDomain.Dto;
using SistemaAcaiDomain.Entities;
using SistemaAcaiRepository.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace SistemaAcaiBusinessTest.Business
{
    [TestFixture]
    public class SaborBusinessTest
    {
        private Mock<ISaborRepository> saborRepository;
        private ISaborBusiness saborBusiness;
        private IMapper mapper;

        [SetUp]
        public void SetUp()
        {
            saborRepository = new Mock<ISaborRepository>();
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            mapper = mapperConfig.CreateMapper();

            saborBusiness = new SaborBusiness(saborRepository.Object, mapper);
        }

        [Test]
        public void DeveRetornarUmaInstanciaDeSabor()
        {
            var entidade = new Sabor
            {
                Id = 1,
                Descricao = "morango",
                TempoPreparo = 3
            };

            saborRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(entidade);
            var sabor = saborBusiness.Get(entidade.Id);

            Assert.AreEqual(entidade.Id, sabor.Id);
            Assert.AreEqual(entidade.Descricao, sabor.Descricao);
            Assert.AreEqual(entidade.TempoPreparo, sabor.TempoPreparo);

            saborRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            saborRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveRetornarVariasIntanciasDeSabor()
        {
            var entidades = new List<Sabor>
            {
                new Sabor
                {
                    Id = 1,
                    Descricao = "morango",
                    TempoPreparo = 3
                },
                new Sabor
                {
                    Id = 2,
                    Descricao = "banana",
                    TempoPreparo = 2
                }
            };

            saborRepository.Setup(s => s.Get()).Returns(entidades);
            var sabores = saborBusiness.Get();

            Assert.AreEqual(entidades.Count(), sabores.Count());

            saborRepository.Verify(v => v.Get(), Times.Once());
            saborRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveExcluirUmaInstanciaDeSabor()
        {
            var entidade = new Sabor
            {
                Id = 1,
                Descricao = "morango",
                TempoPreparo = 3
            };

            saborRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(entidade);
            saborRepository.Setup(s => s.Delete(It.IsAny<Sabor>()));

            saborBusiness.Delete(entidade.Id);
            saborRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            saborRepository.Verify(v => v.Delete(It.IsAny<Sabor>()), Times.Once());
            saborRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveEditarUmaInstanciaDeSabor()
        {
            var entidade = new Sabor
            {
                Id = 1,
                Descricao = "morango",
                TempoPreparo = 3
            };

            var dto = new SaborDto
            {
                Descricao = "banana",
                TempoPreparo = 3
            };

            saborRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(entidade);
            saborRepository.Setup(s => s.Edit(It.IsAny<Sabor>()));

            saborBusiness.Edit(entidade.Id, dto);
            saborRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            saborRepository.Verify(v => v.Edit(It.IsAny<Sabor>()), Times.Once());
            saborRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveInserirUmaInstanciaDeSabor()
        {
            var dto = new SaborDto
            {
                Descricao = "banana",
                TempoPreparo = 3
            };

            saborRepository.Setup(s => s.Insert(It.IsAny<Sabor>()));

            saborBusiness.Insert(dto);

            saborRepository.Verify(v => v.Insert(It.IsAny<Sabor>()), Times.Once());
            saborRepository.VerifyNoOtherCalls();
        }
    }
}
