﻿using AutoMapper;
using Moq;
using NUnit.Framework;
using SistemaAcaiBusiness.AutoMapper;
using SistemaAcaiBusiness.Business;
using SistemaAcaiBusiness.Interfaces;
using SistemaAcaiDomain.Dto;
using SistemaAcaiDomain.Entities;
using SistemaAcaiRepository.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace SistemaAcaiBusinessTest.Business
{
    [TestFixture]
    public class TamanhoBusinessTest
    {
        private Mock<ITamanhoRepository> tamanhoRepository;
        private ITamanhoBusiness tamanhoBusiness;
        private IMapper mapper;

        [SetUp]
        public void SetUp()
        {
            tamanhoRepository = new Mock<ITamanhoRepository>();
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            mapper = mapperConfig.CreateMapper();

            tamanhoBusiness = new TamanhoBusiness(tamanhoRepository.Object, mapper);
        }

        [Test]
        public void DeveRetornarUmaInstanciaDeTamanho()
        {
            var entidade = new Tamanho
            {
                Id = 1,
                Descricao = "pequeno (300ml)",
                TempoPreparo = 5,
                Valor = 7
            };

            tamanhoRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(entidade);
            var tamanho = tamanhoBusiness.Get(entidade.Id);

            Assert.AreEqual(entidade.Id, tamanho.Id);
            Assert.AreEqual(entidade.Descricao, tamanho.Descricao);
            Assert.AreEqual(entidade.TempoPreparo, tamanho.TempoPreparo);
            Assert.AreEqual(entidade.Valor, tamanho.Valor);

            tamanhoRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            tamanhoRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveRetornarVariasIntanciasDeTamanho()
        {
            var entidades = new List<Tamanho>
            {
                new Tamanho
                {
                    Id = 1,
                    Descricao = "pequeno (300ml)",
                    TempoPreparo = 5,
                    Valor = 7
                },
                new Tamanho
                {
                    Id = 2,
                    Descricao = "médio (500ml)",
                    TempoPreparo = 7,
                    Valor = 9
                }
            };

            tamanhoRepository.Setup(s => s.Get()).Returns(entidades);
            var tamanhos = tamanhoBusiness.Get();

            Assert.AreEqual(entidades.Count(), tamanhos.Count());

            tamanhoRepository.Verify(v => v.Get(), Times.Once());
            tamanhoRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveExcluirUmaInstanciaDeTamanho()
        {
            var entidade = new Tamanho
            {
                Id = 1,
                Descricao = "pequeno (300ml)",
                TempoPreparo = 5,
                Valor = 7
            };

            tamanhoRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(entidade);
            tamanhoRepository.Setup(s => s.Delete(It.IsAny<Tamanho>()));

            tamanhoBusiness.Delete(entidade.Id);
            tamanhoRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            tamanhoRepository.Verify(v => v.Delete(It.IsAny<Tamanho>()), Times.Once());
            tamanhoRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveEditarUmaInstanciaDeTamanho()
        {
            var entidade = new Tamanho
            {
                Id = 1,
                Descricao = "pequeno (300ml)",
                TempoPreparo = 5,
                Valor = 7
            };

            var dto = new TamanhoDto
            {
                Descricao = "pequeno (350ml)",
                TempoPreparo = 6,
                Valor = 7
            };

            tamanhoRepository.Setup(s => s.Get(It.IsAny<int>())).Returns(entidade);
            tamanhoRepository.Setup(s => s.Edit(It.IsAny<Tamanho>()));

            tamanhoBusiness.Edit(entidade.Id, dto);
            tamanhoRepository.Verify(v => v.Get(It.IsAny<int>()), Times.Once());
            tamanhoRepository.Verify(v => v.Edit(It.IsAny<Tamanho>()), Times.Once());
            tamanhoRepository.VerifyNoOtherCalls();
        }

        [Test]
        public void DeveInserirUmaInstanciaDeTamanho()
        {
            var dto = new TamanhoDto
            {
                Descricao = "pequeno (350ml)",
                TempoPreparo = 6,
                Valor = 7
            };

            tamanhoRepository.Setup(s => s.Insert(It.IsAny<Tamanho>()));

            tamanhoBusiness.Insert(dto);

            tamanhoRepository.Verify(v => v.Insert(It.IsAny<Tamanho>()), Times.Once());
            tamanhoRepository.VerifyNoOtherCalls();
        }
    }
}