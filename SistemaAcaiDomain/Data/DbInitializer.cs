﻿using SistemaAcaiDomain.Entities;
using System.Linq;

namespace SistemaAcaiDomain.Data
{
    public static class DbInitializer
    {
        public static void Initialize(SistemaAcaiContext context)
        {
            context.Database.EnsureCreated();

            if (context.Sabor.Any())
            {
                return;
            }

            var sabores = new Sabor[]
            {
                new Sabor{ Descricao = "Morango" },
                new Sabor{ Descricao = "Banana" },
                new Sabor{ Descricao = "Kiwi", TempoPreparo = 5 },
            };

            foreach (var sabor in sabores)
            {
                context.Sabor.Add(sabor);
            }

            context.SaveChanges();

            var tamanhos = new Tamanho[]
            {
                new Tamanho{ Descricao = "pequeno (300ml)", TempoPreparo = 5, Valor = 10.0m },
                new Tamanho{ Descricao = "médio (500ml)", TempoPreparo = 7, Valor = 13.0m },
                new Tamanho{ Descricao = "grande (700ml)", TempoPreparo = 10, Valor = 15.0m },
            };

            foreach (var tamanho in tamanhos)
            {
                context.Tamanho.Add(tamanho);
            }

            context.SaveChanges();

            var adicionais = new Adicional[]
            {
                new Adicional{ Descricao = "granola" },
                new Adicional{ Descricao = "paçoca", TempoPreparo = 3, Valor = 3.0m },
                new Adicional{ Descricao = "leite ninho", Valor = 3.0m },
            };

            foreach (var adicional in adicionais)
            {
                context.Adicional.Add(adicional);
            }

            context.SaveChanges();
        }
    }
}
