﻿using Microsoft.EntityFrameworkCore;
using SistemaAcaiDomain.Entities;

namespace SistemaAcaiDomain.Data
{
    public class SistemaAcaiContext : DbContext
    {
        public SistemaAcaiContext(DbContextOptions<SistemaAcaiContext> options) : base(options)
        {
        }

        public DbSet<Tamanho> Tamanho { get; set; }
        public DbSet<Sabor> Sabor { get; set; }
        public DbSet<Adicional> Adicional { get; set; }
        public DbSet<Pedido> Pedido { get; set; }
        public DbSet<PedidoAdicional> PedidoAdicional { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tamanho>().Property(p => p.Descricao).HasMaxLength(50).IsRequired();
            modelBuilder.Entity<Tamanho>().Property(p => p.TempoPreparo).IsRequired();
            modelBuilder.Entity<Tamanho>().Property(p => p.Valor).IsRequired();

            modelBuilder.Entity<Sabor>().Property(p => p.Descricao).HasMaxLength(50).IsRequired();

            modelBuilder.Entity<Adicional>().Property(p => p.Descricao).HasMaxLength(50).IsRequired();

            modelBuilder.Entity<Pedido>().Property(p => p.TamanhoId).IsRequired();
            modelBuilder.Entity<Pedido>().Property(p => p.SaborId).IsRequired();
            modelBuilder.Entity<Pedido>().Property(p => p.TempoPreparo).IsRequired();
            modelBuilder.Entity<Pedido>().Property(p => p.ValorTotal).IsRequired();

            modelBuilder.Entity<PedidoAdicional>().Property(p => p.AdicionalId).IsRequired();
            modelBuilder.Entity<PedidoAdicional>().Property(p => p.PedidoId).IsRequired();

            base.OnModelCreating(modelBuilder);
        }
    }
}
