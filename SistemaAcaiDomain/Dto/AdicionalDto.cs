﻿using System.ComponentModel.DataAnnotations;

namespace SistemaAcaiDomain.Dto
{
    public class AdicionalDto
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Descrição é obrigatória")]
        [MaxLength(50, ErrorMessage = "Tamanho máximo para descrição é de 50 caracteres")]
        public string Descricao { get; set; }
        public int? TempoPreparo { get; set; }
        public decimal? Valor { get; set; }
    }
}
