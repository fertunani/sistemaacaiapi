﻿using System.ComponentModel.DataAnnotations;

namespace SistemaAcaiDomain.Dto
{
    public class PedidoAdicionalDto
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "PedidoId é obrigatório")]
        public int PedidoId { get; set; }
        [Required(ErrorMessage = "AdicionalId é obrigatório")]
        public int AdicionalId { get; set; }
    }
}
