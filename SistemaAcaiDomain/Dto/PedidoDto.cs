﻿using System.ComponentModel.DataAnnotations;

namespace SistemaAcaiDomain.Dto
{
    public class PedidoDto
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "TamanhoId é obrigatório")]
        public int TamanhoId { get; set; }
        [Required(ErrorMessage = "SaborId é obrigatório")]
        public int SaborId { get; set; }
        public int TempoPreparo { get; set; }
        public decimal ValorTotal { get; set; }
    }
}
