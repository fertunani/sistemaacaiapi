﻿using System.ComponentModel.DataAnnotations;

namespace SistemaAcaiDomain.Dto
{
    public class SaborDto
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Descrição é obrigatória")]
        [MaxLength(50, ErrorMessage = "Tamanho máximo para descrição é de 50 caracteres")]
        public string Descricao { get; set; }
        public int? TempoPreparo { get; set; }
    }
}
