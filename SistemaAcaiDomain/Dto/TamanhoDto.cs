﻿using System.ComponentModel.DataAnnotations;

namespace SistemaAcaiDomain.Dto
{
    public class TamanhoDto
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Descrição é obrigatória")]
        [MaxLength(50, ErrorMessage = "Tamanho máximo para descrição é de 50 caracteres")]
        public string Descricao { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Tempo de preparo deve ser maior que 0")]
        public int TempoPreparo { get; set; }
        [Range(0.01d, double.MaxValue, ErrorMessage = "Valor deve ser maior que 0")]
        public decimal Valor { get; set; }
    }
}
