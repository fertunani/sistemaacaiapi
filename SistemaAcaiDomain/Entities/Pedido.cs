﻿namespace SistemaAcaiDomain.Entities
{
    public class Pedido
    {
        public int Id { get; set; }
        public int TamanhoId { get; set; }
        public int SaborId { get; set; }
        public int TempoPreparo { get; set; }
        public decimal ValorTotal { get; set; }

        public Tamanho Tamanho { get; set; }
        public Sabor Sabor { get; set; }
    }
}
