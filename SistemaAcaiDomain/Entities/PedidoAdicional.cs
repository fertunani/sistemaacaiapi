﻿namespace SistemaAcaiDomain.Entities
{
    public class PedidoAdicional
    {
        public int Id { get; set; }
        public int PedidoId { get; set; }
        public int AdicionalId { get; set; }

        public Pedido Pedido { get; set; }
        public Adicional Adicional { get; set; }
    }
}
