﻿using System.Collections.Generic;

namespace SistemaAcaiDomain.Entities.Projections
{
    public class PedidoInfo
    {
        public int Id { get; set; }
        public string Tamanho { get; set; }
        public string Sabor { get; set; }
        public decimal Valor { get; set; }
        public List<AdicionalInfo> Adicionais { get; set; }
        public decimal ValorTotal { get; set; }
        public int TempoPreparo { get; set; }
    }

    public class AdicionalInfo
    {
        public string Adicional { get; set; }
        public decimal? Valor { get; set; }
    }
}
