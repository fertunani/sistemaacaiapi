﻿namespace SistemaAcaiDomain.Entities
{
    public class Sabor
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public int? TempoPreparo { get; set; }
    }
}
