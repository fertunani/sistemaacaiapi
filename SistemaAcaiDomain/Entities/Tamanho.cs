﻿namespace SistemaAcaiDomain.Entities
{
    public class Tamanho
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public int TempoPreparo { get; set; }
        public decimal Valor { get; set; }
    }
}
