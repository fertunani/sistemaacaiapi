﻿using SistemaAcaiDomain.Entities;
using System.Collections.Generic;

namespace SistemaAcaiRepository.Interfaces
{
    public interface IAdicionalRepository
    {
        IEnumerable<Adicional> Get();
        Adicional Get(int id);
        void Edit(Adicional adicional);
        void Insert(Adicional adicional);
        void Delete(Adicional adicional);
    }
}
