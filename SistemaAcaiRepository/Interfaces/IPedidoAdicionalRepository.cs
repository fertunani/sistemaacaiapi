﻿using SistemaAcaiDomain.Entities;
using System.Collections.Generic;

namespace SistemaAcaiRepository.Interfaces
{
    public interface IPedidoAdicionalRepository
    {
        IEnumerable<PedidoAdicional> Get();
        PedidoAdicional Get(int id);
        void Edit(PedidoAdicional pedidoAdicional);
        int Insert(PedidoAdicional pedidoAdicional);
        void Delete(PedidoAdicional pedidoAdicional);
        IEnumerable<PedidoAdicional> GetByPedido(int idPedido);
    }
}
