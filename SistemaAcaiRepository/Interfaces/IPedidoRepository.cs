﻿using SistemaAcaiDomain.Entities;
using System.Collections.Generic;

namespace SistemaAcaiRepository.Interfaces
{
    public interface IPedidoRepository
    {
        IEnumerable<Pedido> Get();
        Pedido Get(int id);
        void Edit(Pedido pedido);
        int Insert(Pedido pedido);
        void Delete(Pedido pedido);
    }
}
