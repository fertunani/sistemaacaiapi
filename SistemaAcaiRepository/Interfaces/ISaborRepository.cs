﻿using SistemaAcaiDomain.Dto;
using SistemaAcaiDomain.Entities;
using System.Collections.Generic;

namespace SistemaAcaiRepository.Interfaces
{
    public interface ISaborRepository
    {
        IEnumerable<Sabor> Get();
        Sabor Get(int id);
        void Edit(Sabor sabor);
        void Insert(Sabor sabor);
        void Delete(Sabor sabor);
    }
}
