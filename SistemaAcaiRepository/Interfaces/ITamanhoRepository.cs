﻿using SistemaAcaiDomain.Entities;
using System.Collections.Generic;

namespace SistemaAcaiRepository.Interfaces
{
    public interface ITamanhoRepository
    {
        IEnumerable<Tamanho> Get();
        Tamanho Get(int id);
        void Edit(Tamanho tamanho);
        void Insert(Tamanho tamanho);
        void Delete(Tamanho tamanho);
    }
}
