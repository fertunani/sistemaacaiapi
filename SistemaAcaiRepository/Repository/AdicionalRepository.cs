﻿using SistemaAcaiDomain.Data;
using SistemaAcaiDomain.Entities;
using SistemaAcaiRepository.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace SistemaAcaiRepository.Repository
{
    public class AdicionalRepository : IAdicionalRepository
    {
        private readonly SistemaAcaiContext context;

        public AdicionalRepository(SistemaAcaiContext context)
        {
            this.context = context;
        }

        public void Delete(Adicional adicional)
        {
            context.Adicional.Remove(adicional);
            context.SaveChanges();
        }

        public void Edit(Adicional adicional)
        {
            context.Adicional.Update(adicional);
            context.SaveChanges();
        }

        public IEnumerable<Adicional> Get()
        {
            return context.Adicional.OrderBy(o => o.Id);
        }

        public Adicional Get(int id)
        {
            return context.Adicional.FirstOrDefault(f => f.Id == id);
        }

        public void Insert(Adicional adicional)
        {
            context.Adicional.Add(adicional);
            context.SaveChanges();
        }
    }
}
