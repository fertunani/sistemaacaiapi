﻿using SistemaAcaiDomain.Data;
using SistemaAcaiDomain.Entities;
using SistemaAcaiRepository.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace SistemaAcaiRepository.Repository
{
    public class PedidoAdicionalRepository : IPedidoAdicionalRepository
    {
        private readonly SistemaAcaiContext context;

        public PedidoAdicionalRepository(SistemaAcaiContext context)
        {
            this.context = context;
        }

        public void Delete(PedidoAdicional pedidoAdicional)
        {
            context.PedidoAdicional.Remove(pedidoAdicional);
            context.SaveChanges();
        }

        public void Edit(PedidoAdicional pedidoAdicional)
        {
            context.PedidoAdicional.Update(pedidoAdicional);
            context.SaveChanges();
        }

        public IEnumerable<PedidoAdicional> Get()
        {
            return context.PedidoAdicional.OrderBy(o => o.Id);
        }

        public PedidoAdicional Get(int id)
        {
            return context.PedidoAdicional.FirstOrDefault(f => f.Id == id);
        }

        public IEnumerable<PedidoAdicional> GetByPedido(int idPedido)
        {
            return context.PedidoAdicional.Where(w => w.PedidoId == idPedido).OrderBy(o => o.Id);
        }

        public int Insert(PedidoAdicional pedidoAdicional)
        {
            context.PedidoAdicional.Add(pedidoAdicional);
            context.SaveChanges();

            return pedidoAdicional.Id;
        }
    }
}
