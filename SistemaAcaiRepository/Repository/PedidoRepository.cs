﻿using SistemaAcaiDomain.Data;
using SistemaAcaiDomain.Entities;
using SistemaAcaiRepository.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace SistemaAcaiRepository.Repository
{
    public class PedidoRepository : IPedidoRepository
    {
        private readonly SistemaAcaiContext context;

        public PedidoRepository(SistemaAcaiContext context)
        {
            this.context = context;
        }

        public void Delete(Pedido pedido)
        {
            context.Pedido.Remove(pedido);
            context.SaveChanges();
        }

        public void Edit(Pedido pedido)
        {
            context.Pedido.Update(pedido);
            context.SaveChanges();
        }

        public IEnumerable<Pedido> Get()
        {
            return context.Pedido.OrderBy(o => o.Id);
        }

        public Pedido Get(int id)
        {
            return context.Pedido.FirstOrDefault(f => f.Id == id);
        }

        public int Insert(Pedido pedido)
        {
            context.Pedido.Add(pedido);
            context.SaveChanges();

            return pedido.Id;
        }
    }
}
