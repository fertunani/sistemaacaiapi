﻿using SistemaAcaiDomain.Data;
using SistemaAcaiDomain.Entities;
using SistemaAcaiRepository.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace SistemaAcaiRepository.Repository
{
    public class SaborRepository : ISaborRepository
    {
        private readonly SistemaAcaiContext context;

        public SaborRepository(SistemaAcaiContext context)
        {
            this.context = context;
        }

        public void Delete(Sabor sabor)
        {
            context.Sabor.Remove(sabor);
            context.SaveChanges();
        }

        public void Edit(Sabor sabor)
        {
            context.Sabor.Update(sabor);
            context.SaveChanges();
        }

        public IEnumerable<Sabor> Get()
        {
            return context.Sabor.OrderBy(o => o.Id);
        }

        public Sabor Get(int id)
        {
            return context.Sabor.FirstOrDefault(f => f.Id == id);
        }

        public void Insert(Sabor sabor)
        {
            context.Sabor.Add(sabor);
            context.SaveChanges();
        }
    }
}
