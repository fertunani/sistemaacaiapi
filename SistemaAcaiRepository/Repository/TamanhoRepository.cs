﻿using SistemaAcaiDomain.Data;
using SistemaAcaiDomain.Entities;
using SistemaAcaiRepository.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace SistemaAcaiRepository.Repository
{
    public class TamanhoRepository : ITamanhoRepository
    {
        private readonly SistemaAcaiContext context;

        public TamanhoRepository(SistemaAcaiContext context)
        {
            this.context = context;
        }

        public void Delete(Tamanho tamanho)
        {
            context.Tamanho.Remove(tamanho);
            context.SaveChanges();
        }

        public void Edit(Tamanho tamanho)
        {
            context.Tamanho.Update(tamanho);
            context.SaveChanges();
        }

        public IEnumerable<Tamanho> Get()
        {
            return context.Tamanho.OrderBy(o => o.Id);
        }

        public Tamanho Get(int id)
        {
            return context.Tamanho.FirstOrDefault(f => f.Id == id);
        }

        public void Insert(Tamanho tamanho)
        {
            context.Tamanho.Add(tamanho);
            context.SaveChanges();
        }
    }
}
