﻿using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using SistemaAcaiDomain.Data;
using SistemaAcaiDomain.Entities;
using SistemaAcaiRepository.Interfaces;
using SistemaAcaiRepository.Repository;
using System.Collections.Generic;
using System.Linq;

namespace SistemaAcaiRepositoryTest.Repository
{
    [TestFixture]
    public class PedidoAdicionalRepositoryTest
    {
        private SistemaAcaiContext context;
        private IPedidoAdicionalRepository pedidoAdicionalRepository;

        [SetUp]
        public void SetUp()
        {
            var options = new DbContextOptionsBuilder<SistemaAcaiContext>()
                .UseInMemoryDatabase(databaseName: "SistemaAcaiTest")
                .Options;

            context = new SistemaAcaiContext(options);
            pedidoAdicionalRepository = new PedidoAdicionalRepository(context);
        }

        [Test]
        public void DeveRetornarAdicionaisDoPedido()
        {
            var entidades = new List<PedidoAdicional>
            {
                new PedidoAdicional
                {
                    Id = 1,
                    AdicionalId = 1,
                    PedidoId = 1
                },
                new PedidoAdicional
                {
                    Id = 2,
                    AdicionalId = 1,
                    PedidoId = 2
                },
            };

            context.PedidoAdicional.AddRange(entidades);
            context.SaveChanges();

            var adicionais = pedidoAdicionalRepository.GetByPedido(1);

            Assert.AreEqual(1, adicionais.Count());
        }
    }
}
